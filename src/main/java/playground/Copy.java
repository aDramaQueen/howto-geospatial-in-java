package playground;

import org.geotools.api.data.*;
import org.geotools.api.feature.simple.SimpleFeature;
import org.geotools.api.feature.simple.SimpleFeatureType;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.util.URLs;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Copy {

    public static void main(String[] args) throws IOException {
        exportToShapefile2(null, "", null);
    }

    private static void exportToShapefile2(DataStore database, String typeName, File directory) throws IOException {
        // Grab existing feature source from database
        SimpleFeatureSource featureSource = database.getFeatureSource(typeName);
        SimpleFeatureType ft = featureSource.getSchema();

        String fileName = ft.getTypeName();
        File file = new File(directory, fileName + ".shp");

        Map<String, java.io.Serializable> creationParams = new HashMap<>();
        creationParams.put("url", URLs.fileToUrl(file));

        FileDataStoreFactorySpi factory = FileDataStoreFinder.getDataStoreFactory("shp");
        DataStore dataStore = factory.createNewDataStore(creationParams);

        dataStore.createSchema(ft);

        SimpleFeatureStore featureStore = (SimpleFeatureStore) dataStore.getFeatureSource(typeName);

        try (Transaction t = new DefaultTransaction()) {
            SimpleFeatureCollection collection = featureSource.getFeatures(); // grab all features

            FeatureWriter<SimpleFeatureType, SimpleFeature> writer =
                    dataStore.getFeatureWriter(typeName, t);

            SimpleFeatureIterator iterator = collection.features();
            SimpleFeature feature;
            try {
                while (iterator.hasNext()) {
                    feature = iterator.next();

                    // Step1: create a new empty feature on each call to next
                    SimpleFeature aNewFeature = writer.next();
                    // Step2: copy the values in
                    aNewFeature.setAttributes(feature.getAttributes());
                    // Step3: write out the feature
                    writer.write();
                }

            } catch (IOException eek) {
                eek.printStackTrace();
                try {
                    t.rollback();
                } catch (IOException doubleEeek) {
                    // rollback failed?
                }
            }
        }
        //return dataStore;
    }

//    public static String createWorldMap() throws IOException {
//
//        URL path = Quickstart.class.getResource("/spatial/ne_10m_admin_0_countries/ne_10m_admin_0_countries.shp");
//
//        if(path == null) {
//            System.out.println("Couldn't find file...");
//        } else {
//            // Load file into file store
//            File file = new File(path.getFile());
//            FileDataStore inputDataStore  = FileDataStoreFinder.getDataStore(file);
//            String inputTypeName = inputDataStore.getTypeNames()[0];
//            SimpleFeatureSource featureSource = inputDataStore.getFeatureSource();
//            SimpleFeatureType inputType = inputDataStore.getSchema(inputTypeName);
//
//            FeatureSource<SimpleFeatureType, SimpleFeature> source = inputDataStore.getFeatureSource(inputTypeName);
//            FeatureCollection<SimpleFeatureType, SimpleFeature> inputFeatureCollection = source.getFeatures();
//
//            DataStore outputDataStore = DataStoreFinder.getDataStore(getH2DefaultParameters());
//            if (outputDataStore == null) {
//                return "Could not connect - check parameters";
//            } else {
//                outputDataStore.createSchema(inputType);
//                SimpleFeatureStore featureStore = (SimpleFeatureStore) outputDataStore.getFeatureSource(inputTypeName);
//                featureStore.addFeatures(source.getFeatures(/*filter*/));
//
//                inputDataStore.dispose();
//                outputDataStore.dispose();
//                outputDataStore.createSchema(inputType);
//                String typeName1 = outputDataStore.getTypeNames()[0];
//
//                SimpleFeatureStore featureStore1 = (SimpleFeatureStore) outputDataStore.getFeatureSource(typeName1);
//                featureStore1.addFeatures(inputFeatureCollection);
//
//                inputDataStore.dispose();
//                outputDataStore.dispose();
//            }
//        }
//        return "";
//    }

    public static Map<String, Object> getH2DefaultParameters() {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Evictor run periodicity", null);
        parameters.put("schema", null);
        parameters.put("encode functions", null);
        parameters.put("Primary key metadata table", null);
        parameters.put("autoserver", null);
        parameters.put("preparedStatements", null);
        parameters.put("Batch insert size", null);
        parameters.put("database", "db/test");
        parameters.put("Session startup SQL", null);
        parameters.put("host", null);
        parameters.put("Estimated extends", null);
        parameters.put("fetch size", null);
        parameters.put("Expose primary keys", null);
        parameters.put("validate connections", null);
        parameters.put("Connection timeout", null);
        parameters.put("Session close-up SQL", null);
        parameters.put("Associations", null);
        parameters.put("Callback factory", null);
        parameters.put("url", "file:db/test");
        parameters.put("port", null);
        parameters.put("passwd", null);
        parameters.put("min connections", null);
        parameters.put("dbtype", "h2gis");
        parameters.put("namespace", null);
        parameters.put("Evictor tests per run", null);
        parameters.put("max connections", null);
        parameters.put("Test while idle", null);
        parameters.put("user", null);
        parameters.put("username", "test");
        parameters.put("Max connection idle time", null);
        return parameters;
    }
}
