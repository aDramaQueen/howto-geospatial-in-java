/*
 *    GeoTools Sample code and Tutorials by Open Source Geospatial Foundation, and others
 *    https://docs.geotools.org
 *
 *    To the extent possible under law, the author(s) have dedicated all copyright
 *    and related and neighboring rights to this software to the public domain worldwide.
 *    This software is distributed without any warranty.
 * 
 *    You should have received a copy of the CC0 Public Domain Dedication along with this
 *    software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */
package playground;

import org.geotools.api.data.FileDataStore;
import org.geotools.api.data.FileDataStoreFinder;
import org.geotools.api.data.SimpleFeatureSource;
import org.geotools.api.style.Style;
import org.geotools.map.FeatureLayer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.swing.JMapFrame;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *     Prompts the user for a shapefile and displays the contents on the screen in a map frame.
 * </p><p>
 *     This is the GeoTools Quickstart application used in documentation and tutorials.
 * </p>
 *
 * @see <a href="https://docs.geotools.org/latest/userguide/tutorial/quickstart/index.html">GeoTools - Quickstart</a>
 */
public class QuickstartCroatia {

    /**
     * <p>
     *     GeoTools Quickstart demo application. Prompts the user for a shapefile and displays its contents
     *     on the screen in a map frame
     * </p><p>
     *     ATTENTION: You have to unzip the "croatia-10-11-2023.zip" file into a directory "croatia-10-11-2023" first!!!
     * </p>
     */
    public static void main(String[] args) throws URISyntaxException {
        if(QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_buildings_a_free_1.shp") == null){
            URI zip = QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023.zip").toURI();
            URI targetDir = QuickstartCroatia.class.getResource("/spatial/").toURI().resolve("croatia-10-11-2023/");
            throw new RuntimeException("%n\tYou have to unzip the '%s' file%n\tinto a directory '%s' first!!!".formatted(Path.of(zip), Path.of(targetDir)));
        }
        List<URL> layers = Arrays.asList(
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_buildings_a_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_landuse_a_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_natural_a_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_natural_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_places_a_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_places_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_pofw_a_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_pofw_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_pois_a_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_pois_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_railways_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_roads_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_traffic_a_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_traffic_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_transport_a_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_transport_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_water_a_free_1.shp"),
            QuickstartCroatia.class.getResource("/spatial/croatia-10-11-2023/gis_osm_waterways_free_1.shp")
        );
        // Create a map
        MapContent map = new MapContent();
        map.setTitle("Croatia");
        layers.forEach(layer -> {
            // Add shapefile to it
            if(layer == null) {
                System.out.println("Couldn't find file...");
            } else {
                File file = new File(layer.getFile());
                try {
                    FileDataStore store = FileDataStoreFinder.getDataStore(file);
                    SimpleFeatureSource featureSource = store.getFeatureSource();

                    Style style = SLD.createSimpleStyle(featureSource.getSchema());
                    map.addLayer(new FeatureLayer(featureSource, style));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        // Now display the map
        JMapFrame.showMap(map);
    }
}
