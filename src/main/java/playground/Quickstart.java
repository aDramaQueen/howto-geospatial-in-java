/*
 *    GeoTools Sample code and Tutorials by Open Source Geospatial Foundation, and others
 *    https://docs.geotools.org
 *
 *    To the extent possible under law, the author(s) have dedicated all copyright
 *    and related and neighboring rights to this software to the public domain worldwide.
 *    This software is distributed without any warranty.
 * 
 *    You should have received a copy of the CC0 Public Domain Dedication along with this
 *    software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */
package playground;

import org.geotools.api.data.FileDataStore;
import org.geotools.api.data.FileDataStoreFinder;
import org.geotools.api.data.SimpleFeatureSource;
import org.geotools.api.style.Style;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.swing.JMapFrame;

import java.io.File;
import java.net.URL;

/**
 * <p>
 *     Prompts the user for a shapefile and displays the contents on the screen in a map frame.
 * </p><p>
 *     This is the GeoTools Quickstart application used in documentation and tutorials.
 * </p>
 *
 * @see <a href="https://docs.geotools.org/latest/userguide/tutorial/quickstart/index.html">GeoTools - Quickstart</a>
 */
public class Quickstart {

    /**
     * GeoTools Quickstart demo application. Prompts the user for a shapefile and displays its
     * contents on the screen in a map frame
     */
    public static void main(String[] args) throws Exception {
        URL path = Quickstart.class.getResource("/spatial/world-borders/ne_10m_admin_0_countries.shp");
        // URL path = Quickstart.class.getResource("/locations/locations.shp");
        if(path == null) {
            System.out.println("Couldn't find file...");
        } else {
            File file = new File(path.getFile());
            FileDataStore store = FileDataStoreFinder.getDataStore(file);
            SimpleFeatureSource featureSource = store.getFeatureSource();

            // Create a map content and add our shapefile to it
            MapContent map = new MapContent();
            map.setTitle("Quickstart");

            Style style = SLD.createSimpleStyle(featureSource.getSchema());
            Layer layer = new FeatureLayer(featureSource, style);
            map.addLayer(layer);

            // Now display the map
            JMapFrame.showMap(map);
        }
    }
}
