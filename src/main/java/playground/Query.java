package playground;

import org.geotools.api.data.DataStore;
import org.geotools.api.data.DataStoreFactorySpi;
import org.geotools.api.data.DataStoreFinder;
import org.geotools.api.data.SimpleFeatureSource;
import org.geotools.api.feature.type.FeatureType;
import org.geotools.api.filter.Filter;
import org.geotools.data.postgis.PostgisNGDataStoreFactory;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.swing.action.SafeAction;
import org.geotools.swing.data.JDataStoreWizard;
import org.geotools.swing.table.FeatureCollectionTableModel;
import org.geotools.swing.wizard.JWizard;
import org.h2gis.geotools.H2GISDataStoreFactory;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.nio.file.Path;
import java.util.Map;

/**
 * <p>
 *     The Query is an excuse to try out Filters and Expressions on your own data with a table to show the results.
 * </p><p>
 *     Remember when programming that you have other options then the CQL parser, you can directly
 *     make a Filter using CommonFactoryFinder.getFilterFactory().
 * </p>
 *
 * @see <a href="https://docs.geotools.org/stable/userguide/tutorial/filter/query.html">GeoTools - Query Tutorial</a>
 */
@SuppressWarnings("serial")
public class Query extends JFrame {

    private DataStore dataStore;
    private JComboBox<String> featureTypeCBox;
    private JTable table;
    private JTextField text;

    public static void main(String[] args) {
        JFrame frame = new Query();
        frame.setVisible(true);
    }

    public Query() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(new BorderLayout());

        text = new JTextField(80);
        text.setText("include"); // include selects everything!
        getContentPane().add(text, BorderLayout.NORTH);

        table = new JTable();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setModel(new DefaultTableModel(5, 5));
        table.setPreferredScrollableViewportSize(new Dimension(500, 200));

        JScrollPane scrollPane = new JScrollPane(table);
        getContentPane().add(scrollPane, BorderLayout.CENTER);

        JMenuBar menubar = new JMenuBar();
        setJMenuBar(menubar);

        JMenu fileMenu = new JMenu("File");
        menubar.add(fileMenu);

        featureTypeCBox = new JComboBox<>();
        menubar.add(featureTypeCBox);

        JMenu dataMenu = new JMenu("Data");
        menubar.add(dataMenu);
        pack();

        fileMenu.add(
                new SafeAction("Open shapefile...") {
                    public void action(ActionEvent e) throws Throwable {
                        connect(new ShapefileDataStoreFactory());
                    }
                });
        fileMenu.add(
                // PostGIS
                new SafeAction("Connect to PostGIS database...") {
                    public void action(ActionEvent e) throws Throwable {
                        connect(new PostgisNGDataStoreFactory());
                    }
                });
        fileMenu.add(
                // H2GIS
                new SafeAction("Connect to embedded H2GIS database...") {
                    public void action(ActionEvent e) throws Throwable {
                        H2GISDataStoreFactory factory = new H2GISDataStoreFactory();
                        Map<String, Object> predefinedParameters = null;
                        try {
                            Path root = Path.of(Query.class.getResource(".").toURI());
                            root = root.getParent().getParent().getParent().getParent().getParent().getParent();
                            File newFile = root.resolve("db/test.mv.db").toFile();
                            newFile.mkdirs();
                            predefinedParameters = Copy.getH2DefaultParameters();
                        } catch (NullPointerException exc) {
                            System.out.println("Couldn't get project directory. Assuming this application runs as JAR...");
                        }
                        connect(factory, predefinedParameters);
                    }
                });
        fileMenu.add(
                new SafeAction("Connect to DataStore...") {
                    public void action(ActionEvent e) throws Throwable {
                        connect(null);
                    }
                });
        fileMenu.addSeparator();
        fileMenu.add(
                new SafeAction("Exit") {
                    public void action(ActionEvent e) throws Throwable {
                        System.exit(0);
                    }
                });

        dataMenu.add(
                new SafeAction("Get features") {
                    public void action(ActionEvent e) throws Throwable {
                        filterFeatures();
                    }
                });
        dataMenu.add(
                new SafeAction("Count") {
                    public void action(ActionEvent e) throws Throwable {
                        countFeatures();
                    }
                });
        dataMenu.add(
                new SafeAction("Geometry") {
                    public void action(ActionEvent e) throws Throwable {
                        queryFeatures();
                    }
                });
    }

    private void connect(DataStoreFactorySpi format) throws Exception {
        connect(format, null);
    }

    private void connect(DataStoreFactorySpi format, Map<String, Object> predefinedParameters) throws Exception {
        if(predefinedParameters == null) {
            JDataStoreWizard wizard = new JDataStoreWizard(format);
            int result = wizard.showModalDialog();
            if (result == JWizard.FINISH) {
                Map<String, Object> connectionParameters = wizard.getConnectionParameters();
                dataStore = DataStoreFinder.getDataStore(connectionParameters);
                if (dataStore == null) {
                    JOptionPane.showMessageDialog(null, "Could not connect - check parameters");
                }
                updateUI();
            }
        } else {
            dataStore = DataStoreFinder.getDataStore(predefinedParameters);
            if (dataStore == null) {
                JOptionPane.showMessageDialog(null, "Could not connect - check parameters");
            }
            updateUI();
        }
    }

    private void updateUI() throws Exception {
        ComboBoxModel<String> cbm = new DefaultComboBoxModel<>(dataStore.getTypeNames());
        featureTypeCBox.setModel(cbm);

        table.setModel(new DefaultTableModel(5, 5));
    }

    private void filterFeatures() throws Exception {
        String typeName = (String) featureTypeCBox.getSelectedItem();
        SimpleFeatureSource source = dataStore.getFeatureSource(typeName);

        Filter filter = CQL.toFilter(text.getText());
        SimpleFeatureCollection features = source.getFeatures(filter);
        FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
        table.setModel(model);
    }

    private void countFeatures() throws Exception {
        String typeName = (String) featureTypeCBox.getSelectedItem();
        SimpleFeatureSource source = dataStore.getFeatureSource(typeName);

        Filter filter = CQL.toFilter(text.getText());
        SimpleFeatureCollection features = source.getFeatures(filter);

        int count = features.size();
        JOptionPane.showMessageDialog(text, "Number of selected features:" + count);
    }

    private void queryFeatures() throws Exception {
        String typeName = (String) featureTypeCBox.getSelectedItem();
        SimpleFeatureSource source = dataStore.getFeatureSource(typeName);

        FeatureType schema = source.getSchema();
        String name = schema.getGeometryDescriptor().getLocalName();

        Filter filter = CQL.toFilter(text.getText());

        org.geotools.api.data.Query query = new org.geotools.api.data.Query(typeName, filter, new String[] {name});

        SimpleFeatureCollection features = source.getFeatures(query);

        FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
        table.setModel(model);
    }

}
